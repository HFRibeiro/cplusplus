#include "stdio.h"
#include <sched.h>
#include <pthread.h>
#include <unistd.h>

#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

using namespace cv;
using namespace std;

void* Thread1(void* dummy)
{
    int count = 0;
    while(1)
    {
        printf("Seconds: %d\n",count);
        count++;
        sleep(1);
    }
}

int main(int argc, char* argv[])
{
    pthread_t tid1;

    pthread_create(&tid1, NULL, Thread1,NULL);

    Mat image;
    image = imread("logo.png", 1);

    if (!image.data) {
        printf("No image data \n");
        return -1;
    }
    namedWindow("Frame", WINDOW_AUTOSIZE);
    imshow("Frame", image);

    waitKey(1000);

    VideoCapture cap(0); // open the default camera
    if(!cap.isOpened())  return -1;

    int frame_width  =   cap.get(CV_CAP_PROP_FRAME_WIDTH);
    int frame_height =   cap.get(CV_CAP_PROP_FRAME_HEIGHT);
    int codec        =   cap.get(CV_CAP_PROP_FOURCC);
    int fps          =   cap.get(CV_CAP_PROP_FPS);

    VideoWriter video("out.avi",CV_FOURCC('M','J','P','G'),fps/2, Size(frame_width,frame_height),true);

    Mat edges;
    namedWindow("edges");
    
    while(waitKey(30) <= 0)
    {
        Mat frame;
        cap >> frame; // get a new frame from camera
        video.write(frame);
        cvtColor(frame, edges, COLOR_BGR2GRAY);
        GaussianBlur(edges, edges, Size(7,7), 1.5, 1.5);
        Canny(edges, edges, 0, 30, 3);
        imshow("edges", edges);
    }

    cap.release();
    video.release();

    return 0;
}
