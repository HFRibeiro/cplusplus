#include "stdio.h"
#include <sched.h>
#include <pthread.h>
#include <unistd.h>

typedef struct tagMY_CONTEXT {
    int count;
} MY_CONTEXT, *PMY_CONTEXT;

void* Thread1(void* context)
{
    MY_CONTEXT* displayContext = (MY_CONTEXT*)context;
    while(1)
    {
        printf("displayContext->count: %d\n",displayContext->count);
        displayContext->count++;
        sleep(1);
    }
    pthread_exit(0);
}

void* Thread2(void* context)
{
    MY_CONTEXT* displayContext = (MY_CONTEXT*)context;
    while(1)
    {
        printf("displayContext->count: %d\n",displayContext->count);
        displayContext->count++;
        usleep(0.5*1000000);//1000000 microseconds = 1 second
    }
    pthread_exit(0);
}


int main(int argc, char* argv[])
{
    pthread_t tid1,tid2;
    MY_CONTEXT context = { 0 };
    
    pthread_create(&tid1, NULL, Thread1, &context);
    pthread_create(&tid2, NULL, Thread2, &context);

    while(context.count < 10);   
    printf("Final second %d\n",context.count);          
    return 0;
}
