#include "stdio.h"
#include "cordef.h"
#include "GenApi/GenApi.h" //!< GenApi lib definitions.
#include "gevapi.h" //!< GEV lib definitions.
#include "FileUtil.h"
#include <sched.h>
#include <pthread.h>
#include <unistd.h>

#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

using namespace cv;
using namespace std;

#define SAVE_VIDEO false

//############################################################//

#define MAX_NETIF 8
#define MAX_CAMERAS_PER_NETIF 32
#define MAX_CAMERAS (MAX_NETIF * MAX_CAMERAS_PER_NETIF)

// Enable/disable Bayer to RGB conversion
// (If disabled - Bayer format will be treated as Monochrome).
#define ENABLE_BAYER_CONVERSION 1

// Enable/disable buffer FULL/EMPTY handling (cycling)
#define USE_SYNCHRONOUS_BUFFER_CYCLING 0

// Enable/disable transfer tuning (buffering, timeouts, thread affinity).
#define TUNE_STREAMING_THREADS 0

#define NUM_BUF 8
void* m_latestBuffer = NULL;

Mat frame;

typedef struct tagMY_CONTEXT {
    GEV_CAMERA_HANDLE camHandle;
    int depth;
    int format;
    void* convertBuffer;
    BOOL convertFormat;
    BOOL exit;
} MY_CONTEXT, *PMY_CONTEXT;

//############################################################//

void* ImageDisplayThread(void* context)
{
    MY_CONTEXT* displayContext = (MY_CONTEXT*)context;

    int count_images = 0;
    printf("Start ImageDisplayThread\n");

    if (displayContext != NULL) 
	{
        // While we are still running.
        while (!displayContext->exit) 
		{
            GEV_BUFFER_OBJECT* img = NULL;
            GEV_STATUS status = 0;

            // Wait for images to be received
            status = GevWaitForNextImage(displayContext->camHandle, &img,1000);

            if ((img != NULL) && (status == GEVLIB_OK)) 
			{
                if (img->status == 0) 
				{
                    m_latestBuffer = img->address;
                    count_images++;
                    //printf("count_images: %d\n",count_images);
                }
                else 
				{
                    printf("Error getting image\n");
                    // Image had an error (incomplete (timeout/overflow/lost)).
                    // Do any handling of this condition necessary.
                }
				
            }
#if USE_SYNCHRONOUS_BUFFER_CYCLING
            if (img != NULL) 
			{
                // Release the buffer back to the image transfer process.
                GevReleaseImage(displayContext->camHandle, img);
            }
#endif
        }
    }
    printf("Exit ImageDisplayThread\n");
    pthread_exit(0);
}

int main(int argc, char* argv[])
{

    //##################################################//


    GEV_DEVICE_INTERFACE pCamera[MAX_CAMERAS] = { 0 };
    GEV_STATUS status;
    int numCamera = 0;
    int camIndex = 0;
    //X_VIEW_HANDLE View = NULL;
    MY_CONTEXT context = { 0 };
    pthread_t tid,tid2;
    char c;
    int done = FALSE;
    int turboDriveAvailable = 0;
    char uniqueName[128];
    uint32_t macLow = 0; // Low 32-bits of the mac address (for file naming).

    {
        GEVLIB_CONFIG_OPTIONS options = { 0 };

        GevGetLibraryConfigOptions(&options);
        //options.logLevel = GEV_LOG_LEVEL_OFF;
        //options.logLevel = GEV_LOG_LEVEL_TRACE;
        options.logLevel = GEV_LOG_LEVEL_NORMAL;
        GevSetLibraryConfigOptions(&options);
        printf("set default\n");
    }

    status = GevGetCameraList(pCamera, MAX_CAMERAS, &numCamera);

    uint32 ipAddress = pCamera->ipAddr;
    char ipAddr[16];
    snprintf(ipAddr, sizeof ipAddr, "%u.%u.%u.%u", (ipAddress & 0xff000000) >> 24, (ipAddress & 0x00ff0000) >> 16, (ipAddress & 0x0000ff00) >> 8, (ipAddress & 0x000000ff));

    printf("Camera IP Adress: \033[0;32m%s\033[0m\n", ipAddr);

    printf("%d camera(s) on the network\n", numCamera);



    //====================================================================
            // Connect to Camera
            //
            //
            int i;
            int type;
            UINT32 height = 0;
            UINT32 width = 0;
            UINT32 format = 0;
            UINT32 maxHeight = 1600;
            UINT32 maxWidth = 2048;
            UINT32 maxDepth = 2;
            UINT64 size;
            UINT64 payload_size;
            int numBuffers = NUM_BUF;
            PUINT8 bufAddress[NUM_BUF];
            GEV_CAMERA_HANDLE handle = NULL;
            UINT32 pixFormat = 0;
            UINT32 pixDepth = 0;
            UINT32 convertedGevFormat = 0;

            //====================================================================
            // Open the camera.
            status = GevOpenCamera(&pCamera[camIndex], GevExclusiveMode, &handle);
            if (status == 0) {
                //=================================================================
                // GenICam feature access via Camera XML File enabled by "open"
                //
                // Get the name of XML file name back (example only - in case you need it somewhere).
                //
                char xmlFileName[MAX_PATH] = { 0 };
                status = GevGetGenICamXML_FileName(handle, (int)sizeof(xmlFileName), xmlFileName);
                if (status == GEVLIB_OK) {
                    printf("XML stored as %s\n", xmlFileName);
                }
                status = GEVLIB_OK;
            }
            // Get the low part of the MAC address (use it as part of a unique file name for saving images).
            // Generate a unique base name to be used for saving image files
            // based on the last 3 octets of the MAC address.
            macLow = pCamera[camIndex].macLow;
            macLow &= 0x00FFFFFF;
            snprintf(uniqueName, sizeof(uniqueName), "img_%06x", macLow);

            // Go on to adjust some API related settings (for tuning / diagnostics / etc....).
            if (status == 0) 
        {
                GEV_CAMERA_OPTIONS camOptions = { 0 };

                // Adjust the camera interface options if desired (see the manual)
                GevGetCameraInterfaceOptions(handle, &camOptions);
                //camOptions.heartbeat_timeout_ms = 60000;		// For debugging (delay camera timeout while in debugger)
                camOptions.heartbeat_timeout_ms = 5000; // Disconnect detection (5 seconds)

#if TUNE_STREAMING_THREADS
                // Some tuning can be done here. (see the manual)
                camOptions.streamFrame_timeout_ms = 1001; // Internal timeout for frame reception.
                camOptions.streamNumFramesBuffered = 4; // Buffer frames internally.
                camOptions.streamMemoryLimitMax = 64 * 1024 * 1024; // Adjust packet memory buffering limit.
                camOptions.streamPktSize = 9180; // Adjust the GVSP packet size.
                camOptions.streamPktDelay = 10; // Add usecs between packets to pace arrival at NIC.

                // Assign specific CPUs to threads (affinity) - if required for better performance.
                {
                    int numCpus = _GetNumCpus();
                    if (numCpus > 1) {
                        camOptions.streamThreadAffinity = numCpus - 1;
                        camOptions.serverThreadAffinity = numCpus - 2;
                    }
                }
#endif
                // Write the adjusted interface options back.
                GevSetCameraInterfaceOptions(handle, &camOptions);

                //=====================================================================
                // Get the GenICam FeatureNodeMap object and access the camera features.
                GenApi::CNodeMapRef* Camera = static_cast<GenApi::CNodeMapRef*>(GevGetFeatureNodeMap(handle));

                if (Camera) {
                    // Access some features using the bare GenApi interface methods
                    try {
                        //Mandatory features....
                        GenApi::CIntegerPtr ptrIntNode = Camera->_GetNode("Width");
                        width = (UINT32)ptrIntNode->GetValue();
                        ptrIntNode = Camera->_GetNode("Height");
                        height = (UINT32)ptrIntNode->GetValue();
                        ptrIntNode = Camera->_GetNode("PayloadSize");
                        payload_size = (UINT64)ptrIntNode->GetValue();
                        GenApi::CEnumerationPtr ptrEnumNode = Camera->_GetNode("PixelFormat");
                        format = (UINT32)ptrEnumNode->GetIntValue();
                    }
                    // Catch all possible exceptions from a node access.
                    CATCH_GENAPI_ERROR(status);
                }

                if (status == 0) {
                    //=================================================================
                    // Set up a grab/transfer from this camera
                    //
                    printf("Camera ROI set for:\nHeight = %d\nWidth = %d\nPixelFormat (val) = 0x%08x\n", height, width, format);

                    maxHeight = height;
                    maxWidth = width;
                    maxDepth = GetPixelSizeInBytes(format);

                    // Allocate image buffers
                    // (Either the image size or the payload_size, whichever is larger - allows for packed pixel formats).
                    size = maxDepth * maxWidth * maxHeight;
                    size = (payload_size > size) ? payload_size : size;
                    for (i = 0; i < numBuffers; i++) {
                        bufAddress[i] = (PUINT8)malloc(size);
                        memset(bufAddress[i], 0, size);
                    }

#if USE_SYNCHRONOUS_BUFFER_CYCLING
                    // Initialize a transfer with synchronous buffer handling.
                    status = GevInitializeTransfer(handle, SynchronousNextEmpty, size, numBuffers, bufAddress);
#else
                    // Initialize a transfer with asynchronous buffer handling.
                    status = GevInitializeTransfer(handle, Asynchronous, size, numBuffers, bufAddress);
#endif

    context.camHandle = handle;
    context.exit = FALSE;
    pthread_create(&tid, NULL, ImageDisplayThread, &context);

    //############### Start grabbing ##############################/
    for (i = 0; i < numBuffers; i++)  memset(bufAddress[i], 0, size);
    status = GevStartTransfer(handle, -1);
    if (status != 0) printf("Error starting grab - 0x%x  or %d\n", status, status);
    else printf("Start grabbing\n");

        }
    }

    //====================================================================

    //#################################################//

    Mat image;
    image = imread("logo.png", 1);

    if (!image.data) {
        printf("No image data \n");
        return -1;
    }
    namedWindow("Frame", WINDOW_AUTOSIZE);
    imshow("Frame", image);

    waitKey(1000);
    destroyWindow("Frame");

    printf("Buffer format: %d\n",format);

    namedWindow("Dalsa");

    bool first = false;

    VideoCapture cap(0); // open the default camera
    if(!cap.isOpened())  return -1;

    #if SAVE_VIDEO
    VideoWriter oVideoWriter ("MyVideo.avi",CV_FOURCC('M','J','P','G'), 30, Size(width,height));
    #endif
    
    while(waitKey(30) <= 0)
    {
        //#############################################################

        char filename[128] = { 0 };
        int ret = -1;
        uint32_t saveFormat = format;
        void* bufToSave = m_latestBuffer;
        int allocate_conversion_buffer = 0;
        UINT32 convertedFmt = 0;

        // Make sure we have data to save.
        if (m_latestBuffer != NULL) 
        {
            uint32_t component_count = 1;

            // Bayer conversion enabled for save image to file option.
            //
            // Get the converted pixel type received from the API that is
            //	based on the pixel type output from the camera.
            // (Packed formats are automatically unpacked - unless in "passthru" mode.)
            //
            convertedFmt = GevGetConvertedPixelType(0, format);

            if (GevIsPixelTypeBayer(convertedFmt) && ENABLE_BAYER_CONVERSION) 
            {
                int img_size = 0;
                int img_depth = 0;
                uint8_t fill = 0;

                // Bayer will be converted to RGB.
                saveFormat = GevGetBayerAsRGBPixelType(convertedFmt);

                // Convert the image to RGB.
                img_depth = GevGetPixelDepthInBits(saveFormat);
                component_count = GevGetPixelComponentCount(saveFormat);
                img_size = width * height * component_count * ((img_depth + 7) / 8);
                bufToSave = malloc(img_size);
                fill = (component_count == 4) ? 0xFF : 0; // Alpha if needed.
                memset(bufToSave, fill, img_size);
                allocate_conversion_buffer = 1;

                // Convert the Bayer to RGB
                ConvertBayerToRGB(0, height, width, convertedFmt, m_latestBuffer, saveFormat, bufToSave);
            }
            else 
            {
                saveFormat = convertedFmt;
                allocate_conversion_buffer = 0;
            }

             //#############################################################
             
            Mat img_cam(height, width, CV_8UC1, m_latestBuffer);
            imshow("Dalsa", img_cam);
            #if SAVE_VIDEO
            Mat colorFrame;
            cvtColor(img_cam, colorFrame, CV_GRAY2BGR);
            oVideoWriter.write(colorFrame);
            #endif

        }

    }

    #if SAVE_VIDEO
    oVideoWriter.release();
    #endif

    return 0;
}
